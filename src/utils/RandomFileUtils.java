package utils;

import view.Display;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Definición de los diferentes métodos para ficheros de acceso aleatorio.
 *
 * @author Raúl Tejedor García
 * @version 1.0.0
 * @since 1.0.0
 */
public final class RandomFileUtils {

    /**
     * Constructor por defecto sin argumentos.
     */
    private RandomFileUtils() {
    }

    /**
     * Crea un fichero de acceso aleatorio a partir de un fichero de texto.
     *
     * @param fi Fichero de por defecto de texto.
     * @param wordsDefaultList Lista para almacenar las palabras del fichero de
     * por defecto de texto.
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void createFileFirstTime(File fi, ArrayList<String> wordsDefaultList) throws FileNotFoundException, IOException {
        RandomAccessFile file = new RandomAccessFile(fi, "rw");
        StringBuffer buffer = null;
        for (int i = 0; i < wordsDefaultList.size(); i++) {
            //ID
            file.writeInt(i + 1);
            //Palabra
            buffer = new StringBuffer(checkWordFile(wordsDefaultList.get(i)));
            buffer.setLength(Constants.LENGTH_MAX_WORD);
            file.writeChars(buffer.toString());
        }
        file.close();
    }

    /**
     * Devuelve una palabra tras leer un fichero de acceso aleatorio.
     *
     * @param fi Fichero de acceso aleatorio.
     * @return La palabra a adivinar en el juego.
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static String readWordRandomFile(File fi) throws FileNotFoundException, IOException {
        RandomAccessFile file = new RandomAccessFile(fi, "r");
        char code[] = new char[Constants.LENGTH_MAX_WORD];
        char aux;
        int id = (int) ((Math.random() * getNWordsFile(file)) + 1);
        int pos = Constants.SIZE_WORD * (id - 1);
        String word = "";
        if (pos < file.length()) {
            file.seek(pos);
            id = file.readInt();
            for (int i = 0; i < code.length; i++) {
                aux = file.readChar();
                code[i] = aux;
            }
            word = new String(code);
            //La palabra contiene tambíen los espacios blancos así que los cortamos:
            word = word.trim().toLowerCase();
//            if (id == 0) {
//                Display.writeln("Ese registro no existe=> hay un hueco");
//            } else {
//            }
            //Display.writeln("Palabra " + id + ": " + palabra);
        } else {
            Display.writeln("--> El ID obtenido no está en el fichero");
        }
        file.close();
        return word;
    }

    /**
     * Añade una palabra al fichero de acceso aleatorio personalizado.
     *
     * @param fi Fichero de acceso aleatorio.
     * @param wordsCustomList Lista de palabras del fichero personalizado.
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void addWordRandomFile(File fi, ArrayList<String> wordsCustomList) throws FileNotFoundException, IOException {
        RandomAccessFile file = new RandomAccessFile(fi, "rw");
        StringBuffer buffer = null;
        String word;
        do {
            word = AddQuestionUtils.askWord();
            if (wordsCustomList.contains(word)) {
                Display.writeln("\n--> Introduce una palabra no repetida\n");
            }
        } while (wordsCustomList.contains(word));
        //Al final
        file.seek(file.length());
        file.writeInt(getNWordsFile(file) + 1);
        buffer = new StringBuffer(word);
        buffer.setLength(Constants.LENGTH_MAX_WORD);
        file.writeChars(buffer.toString());
        file.close();
        Display.writeln("\n--> Palabra guardada con éxito");
        Display.writeln("\n---------");
    }

    /**
     * Lee todas las palabras del fichero de acceso aleatorio.
     *
     * @param fi Fichero de acceso aleatorio.
     * @param wordsCustomList Lista de palabras del fichero.
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void readWordsRandomFile(File fi, ArrayList<String> wordsCustomList) throws FileNotFoundException, IOException {
        RandomAccessFile file = new RandomAccessFile(fi, "r");
        Display.writeln("\nPalabras actuales: " + getNWordsFile(file));
        Display.writeln("---------");
        if (getNWordsFile(file) != 0) {
            int pos = 0;
            int id;
            char code[] = new char[Constants.LENGTH_MAX_WORD];
            char aux;
            int y = 0;
            for (;;) {
                //Al principio
                file.seek(pos);
                id = file.readInt();
                for (int i = 0; i < code.length; i++) {
                    aux = file.readChar();
                    code[i] = aux;
                }
                String word = new String(code);
                //La palabra contiene tambíen los espacios blancos así que los cortamos:
                word = word.trim().toLowerCase();
                //Mostar desde fichero sin ordenar
                if (!(y == Constants.SPACING_COLUMNS)) {
                    Display.writef(true, Constants.DEFAULT_SPACING, false, word);
                    y++;
                } else {
                    Display.writef(true, Constants.DEFAULT_SPACING, true, word);
                    y = 0;
                }
                //Añadimos las palabras aquí
                wordsCustomList.add(word);
                //Leemos por posición
                pos = pos + Constants.SIZE_WORD;
                if (file.getFilePointer() == file.length()) {
                    break;
                }
            }
            file.close();
            if (y == 0) {
                Display.writeln("---------");
            } else {
                Display.writeln("\n---------");
            }
        }
    }

    /**
     * Devuelve el número de palabras del fichero de acceso aleatorio.
     *
     * @param file Fichero de acceso aleatorio.
     * @return Número de palabras del fichero.
     * @throws IOException
     */
    public static int getNWordsFile(RandomAccessFile file) throws IOException {
        return (int) (file.length() / Constants.SIZE_WORD);
    }

    /**
     * Devuelve una palabra corregida de un fichero de acceso aleatorio.
     *
     * @param word Palabra a comprobar.
     * @return Palabra corregida.
     */
    public static String checkWordFile(String word) {
        word = word.toLowerCase();
        Pattern pat = Pattern.compile("[a-zñáéíóúü]+");
        Matcher mat = pat.matcher(word);
        String output = word;
        if (!mat.matches()) {
            //á é í ó ú ñ - No las vamos a sustituir
            String original = "àäèëìïòöùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙçÇ¿!¡^`-,_{}[]%&$@|º´+.=#'1234567890";
            String ascii = "aaeeiioouunAAAEEEIIIOOOUU                                    ";
            for (int i = 0; i < original.length(); i++) {
                output = output.replace(original.charAt(i), ascii.charAt(i));
            }
            output = output.replaceAll(" ", "");
        }
        return output;
    }
}
