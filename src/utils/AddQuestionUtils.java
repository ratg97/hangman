package utils;

import view.Display;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Definición de los diferentes métodos para añadir palabras a un fichero.
 *
 * @author Raúl Tejedor García
 * @version 1.0.0
 * @since 1.0.0
 */
public class AddQuestionUtils {

    /**
     * Constructor por defecto sin argumentos.
     */
    private AddQuestionUtils() {
    }

    /**
     * Obtiene la palabra para añadir en el fichero personalizado.
     *
     * @return La palabra en cuestión.
     */
    public static String askWord() {
        String word;
        do {
            Display.write("Introduce una palabra no repetida y " + Constants.REGEX_WORD + ": ");
            //Para chars especiales: ñ, á
            word = new Scanner((new InputStreamReader(
                    System.in, Charset.forName("ISO-8859-1")))).nextLine();
            if (!isWord(word)) {
                Display.writeln("\n--> Introduce una palabra correcta\n");
            }
        } while (!isWord(word));
        return word;
    }

    /**
     * Comprobamos que la la palabra cumple una expresión regular.
     *
     * @param word Palabra introducida.
     * @return true si la palabra cumple esa exp.regular, false si no.
     */
    private static boolean isWord(String word) {
        Pattern pat = Pattern.compile(Constants.REGEX_WORD);
        Matcher mat = pat.matcher(word);
        return mat.matches();
    }
}
