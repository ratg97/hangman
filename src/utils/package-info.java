/**
 * Clases de apoyo para el funcionamiento del juego.
 *
 * @author Raúl Tejedor García
 * @version 1.0.0
 * @since 1.0.0
 */
package utils;
