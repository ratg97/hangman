package utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Definición de los diferentes métodos para ficheros de texto.
 *
 * @author Raúl Tejedor García
 * @version 1.0.0
 * @since 1.0.0
 */
public final class TextFileUtils {

    /**
     * Constructor por defecto sin argumentos.
     */
    private TextFileUtils() {
    }

    /**
     * Añade a un ArrayList el contenido de un fichero de texto.
     *
     * @param wordsDefaultList ArrayList a llenar.
     */
    public static void readTextFile(ArrayList<String> wordsDefaultList) {
        FileReader file = null;
        BufferedReader read = null;
        try {
            file = new FileReader(Constants.DEFAULT_TEXT_FILE_LOAD);
            read = new BufferedReader(file);
            String line;
            while ((line = read.readLine()) != null) {
                wordsDefaultList.add(line);
            }
            read.close();
        } catch (FileNotFoundException io) {
            System.out.println("--> Fichero por defecto no encontrado");
            System.exit(0);
        } catch (IOException en) {
            System.out.println("--> Error de e/s");
        } finally {
            try {
                if (read != null) {
                    read.close();
                }
            } catch (Exception ex) {
                ex.getMessage();
            }
        }
    }
}
