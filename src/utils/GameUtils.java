package utils;

import view.Display;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Definición de los diferentes métodos durante el juego.
 *
 * @author Raúl Tejedor García
 * @version 1.0.0
 * @since 1.0.0
 */
public class GameUtils {

    /**
     * Constructor por defecto sin argumentos.
     */
    private GameUtils() {
    }

    /**
     * Obtiene la letra a usar por el jugador.
     *
     * @return La letra elegida.
     */
    public static char askCharacter() {
        String character = "";
        do {
            Display.write("Introduce una letra " + Constants.REGEX_CHARACTER + ": ");
            //Para chars especiales: ñ, á,...
            character = new Scanner(new InputStreamReader(
                    System.in, Charset.forName("ISO-8859-1"))).nextLine().trim();
            if (!(isCharacter(character))) {
                Display.writeln("\n--> Introduce un valor correcto\n");
            }
        } while (!(isCharacter(character)));
        return character.charAt(0);
    }

    /**
     * Comprobamos que la letra cumple una expresión regular.
     *
     * @param character Letra introducida.
     * @return true si la letra se encuentra en esa exp.regular, false si no
     * está.
     */
    private static boolean isCharacter(String character) {
        Pattern pat = Pattern.compile(Constants.REGEX_CHARACTER);
        Matcher mat = pat.matcher(character);
        return mat.matches();
    }

    /**
     * Indica si se quiere repetir o no la partida con las mismas
     * características.
     *
     * @return true si quiere repetir, false si no quiere.
     */
    public static boolean repeat() {
        boolean rightAnswer;
        int repeatOperation = Constants.LOOP_VALUE;
        Display.writeln("---------");
        Display.writeln("¿Otra partida?");
        Display.writeln(" " + Constants.REPEAT_VALUE + ":" + Constants.REPEAT_NAME);
        Display.writeln(" " + Constants.NO_REPEAT_VALUE + ":" + Constants.NO_REPEAT_NAME);
        Display.writeln("---------\n");
        do {
            try {
                do {
                    Display.write("--> Introduce tu opción: ");
                    repeatOperation = new Scanner(System.in).nextInt();
                    if (repeatOperation != Constants.REPEAT_VALUE && repeatOperation != Constants.NO_REPEAT_VALUE) {
                        Display.writeln("\nIntroduce un valor correcto\n");
                    }
                } while (repeatOperation != Constants.REPEAT_VALUE && repeatOperation != Constants.NO_REPEAT_VALUE);
                rightAnswer = true;
            } catch (final Exception e) {
                rightAnswer = false;
                Display.writeln("\nIntroduce un valor correcto\n");
            }
        } while (!rightAnswer);
        return repeatOperation == Constants.REPEAT_VALUE;
    }

    /**
     * Añade todas las letras posibles para el desarrollo del juego.
     *
     * @param charactersAvailable Lista de los carácteres permitidos.
     */
    public static void addCharactersAvailable(ArrayList<Character> charactersAvailable) {
        int ascii = Constants.MIN_ASCII;
        while (ascii != Constants.MAX_ASCII + 1) {
            charactersAvailable.add((char) ascii);
            //110 la n
            if (ascii == 110) {
                charactersAvailable.add(((Constants.NN + "").toLowerCase().charAt(0)));
            }
            ascii++;
        }
    }

    /**
     * Comprueba si la letra introducida no está usada.
     *
     * @param characterUsed Letra introducida por el jugador.
     * @param usedCharacters Lista de las letras usadas durtante la partida.
     * @return true si está ya usada, false si aún no se ha dicho.
     */
    public static boolean checkUsedCharacters(char characterUsed, ArrayList<Character> usedCharacters) {
        for (int i = 0; i < usedCharacters.size(); i++) {
            if (usedCharacters.get(i) == characterUsed) {
                return true;
            }
        }
        return false;
    }

    /**
     * Muestra las restantes letras que se pueden introducir.
     *
     * @param charactersUsed Listas de las letras usadas.
     * @param charactersAvailable Listas de las letras permitidas en el juego.
     */
    public static void showUsedCharacters(ArrayList<Character> charactersUsed, ArrayList<Character> charactersAvailable) {
        Display.write("Letras posibles: ");
        for (int i = 0; i < charactersUsed.size(); i++) {
            if (charactersAvailable.contains(charactersUsed.get(i))) {
                charactersAvailable.set(charactersAvailable.indexOf(charactersUsed.get(i)), Constants.USED_CHARACTER);
            }
        }
        for (final Character mChar : charactersAvailable) {
            Display.write(mChar + " ");
        }
        Display.writeln("");
    }

    /**
     * Añade letras a una lista en la que se compararán con la palabra.
     *
     * @param character Letra introducida a evaluar.
     * @param charactersIntroduced Lista con las letras introducidas por cada
     * intento.
     */
    public static void addIntroducedCharacters(char character, ArrayList<Character> charactersIntroduced) {
        switch (character) {
            case 'a':
                charactersIntroduced.add(character);
                charactersIntroduced.add('á');
                break;
            case 'e':
                charactersIntroduced.add(character);
                charactersIntroduced.add('é');
                break;
            case 'i':
                charactersIntroduced.add(character);
                charactersIntroduced.add('í');
                break;
            case 'o':
                charactersIntroduced.add(character);
                charactersIntroduced.add('ó');
                break;
            case 'u':
                charactersIntroduced.add(character);
                charactersIntroduced.add('ú');
                charactersIntroduced.add('ü');
                break;
            default:
                charactersIntroduced.add(character);
                break;
        }
    }
}
