package utils;

import java.io.File;

/**
 * Constantes comunes a la aplicación.
 * 
 * @author Raúl Tejedor García
 * @version 1.0.0
 * @since 1.0.0
 */
public class Constants {

    /**
     * Nombre del juego en cuestión.
     */
    public static final String NAME = "AHORCADO";
    /**
     * Fichero por defecto de texto.
     */
    public static final File DEFAULT_TEXT_FILE_LOAD = new File("spanish.txt");
    /**
     * Fichero por defecto de acceso aleatorio.
     */
    public static final File DEFAULT_RANDOM_FILE = new File("spanish.dat");
    /**
     * Fichero personalizado.
     */
    public static final File CUSTOM_FILE = new File("fichero.dat");
    /**
     * Nombre del fichero por defecto al elegir modo.
     */
    public static final String DEFAULT_RANDOM_FILE_NAME = "Español";
    /**
     * Valor del fichero por defecto al elegir modo.
     */
    public static final int DEFAULT_RANDOM_FILE_VALUE = 1;
    /**
     * Nombre del fichero personalizado al elegir modo.
     */
    public static final String CUSTOM_FILE_NAME = "Lista personalizada";
    /**
     * Valor del fichero personalizado al elegir modo.
     */
    public static final int CUSTOM_FILE_VALUE = 2;
    /**
     * Valor para entrar en los bucles.
     */
    public static final int LOOP_VALUE = -1;
    /**
     * Nombre para jugar en el menú.
     */
    public static final String PLAY_NAME = "Jugar";
    /**
     * Valor para jugar en el menú.
     */
    public static final int PLAY_VALUE = 1;
    /**
     * Nombre para añadir palabras al fichero personalizado.
     */
    public static final String ADDWORDS_NAME = "Añadir palabras";
    /**
     * Valor para añadir palabras al fichero personalizado.
     */
    public static final int ADDWORDS_VALUE = 2;
    /**
     * Nombre para ver las intrucciones del juego.
     */
    public static final String INSTRUCTIONS_NAME = "Instrucciones";
    /**
     * Valor para ver las intrucciones del juego.
     */
    public static final int INSTRUCTIONS_VALUE = 3;
    /**
     * Nombre para salir del juego.
     */
    public static final String EXIT_NAME = "Salir";
    /**
     * Valor para salir del juego.
     */
    public static final int EXIT_VALUE = 0;
    /**
     * Nombre de la dificultad fácil.
     */
    public static final String EASY_NAME = "Fácil";
    /**
     * Valor de la dificultad fácil.
     */
    public static final int EASY_VALUE = 1;
    /**
     * Intentos si se elige la dificultad fácil.
     */
    public static final int EASY_ATTEMPTS = 4;
    /**
     * Nombre de la dificultad media.
     */
    public static final String MEDIUM_NAME = "Medio";
    /**
     * Valor de la dificultad media.
     */
    public static final int MEDIUM_VALUE = 2;
    /**
     * Intentos si se elige la dificultad media.
     */
    public static final int MEDIUM_ATTEMPTS = 3;
    /**
     * Nombre de la dificultad dificil.
     */
    public static final String HARD_NAME = "Difícil";
    /**
     * Valor de la dificultad dificil.
     */
    public static final int HARD_VALUE = 3;
    /**
     * Intentos si se elige la dificultad difícil.
     */
    public static final int HARD_ATTEMPTS = 2;
    /**
     * Nombre de la opción para repetir el juego.
     */
    public static final String REPEAT_NAME = "Repetir";
    /**
     * Valor de la opción de repetir el juego.
     */
    public static final int REPEAT_VALUE = 1;
    /**
     * Nombre de la opción para no repetir el juego.
     */
    public static final String NO_REPEAT_NAME = "Regresar al Menú";
    /**
     * Valor de la opción para no repetir el juego.
     */
    public static final int NO_REPEAT_VALUE = 0;
    /**
     * Logitud mínima de las palabras del juego.
     */
    public static final int LENGTH_MIN_WORD = 1;
    /**
     * Logitud máxima permitida de las palabras del juego.
     */
    public static final int LENGTH_MAX_WORD = 24;
    /**
     * Tamaño de cada registro: 2*(longitud máxima de la palabra) + 4.
     */
    public static final int SIZE_WORD = (LENGTH_MAX_WORD * 2) + 4;
    /**
     * Espaciado por defecto entre palabras.
     */
    public static final int DEFAULT_SPACING = LENGTH_MAX_WORD + 1;
    /**
     * Espaciado máximo entre palabras.
     */
    public static final int SPACING_MAX = 50;
    /**
     * Columnas que dividen las palabras.
     */
    public static final int SPACING_COLUMNS = 2;
    /**
     * Valor mínimo ascii permitido en el juego.
     */
    public static final int MIN_ASCII = 97;
    /**
     * Valor máximo ascii permitido en el juego.
     */
    public static final int MAX_ASCII = 122;
    /**
     * Carácter especial de la lengua española.
     */
    public static final char NN = 'ñ';
    /**
     * Carácter para ocultar el real.
     */
    public static final char ENCRYPTED_CHARACTER = '_';
    /**
     * Carácter para bloquear el abc.
     */
    public static final char USED_CHARACTER = '#';
    /**
     * Expresión regular permitida para escribir una palabra.
     */
    public static final String REGEX_WORD = "[a-zñáéíóúü]{" + LENGTH_MIN_WORD + "," + LENGTH_MAX_WORD + "}";
    /**
     * Expresión regular permitida para escribir una letra.
     */
    public static final String REGEX_CHARACTER = "[a-zñ]{" + LENGTH_MIN_WORD + "," + LENGTH_MIN_WORD + "}";
}
