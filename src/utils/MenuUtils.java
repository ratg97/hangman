package utils;

import controller.GameController;
import java.io.IOException;
import java.util.ArrayList;
import view.Display;
import java.util.Scanner;


/**
 * Definición de los diferentes métodos en el menú.
 *
 * @author Raúl Tejedor García
 * @version 1.0.0
 * @since 1.0.0
 */
public class MenuUtils {

    /**
     * Constructor por defecto sin argumentos.
     */
    private MenuUtils() {
    }

    /**
     * Muestra las opciones del juego.
     *
     * @param game El juego en cuestión.
     * @throws IOException
     */
    public static void showOptions(GameController game) throws IOException {
        int operationMenu;
        do {
            operationMenu = MenuUtils.getOperation();
            switch (operationMenu) {
                case Constants.PLAY_VALUE:
                    game.addSettigns();
                    if (game.isSettingsRight()) {
                        game.play();
                    }
                    break;
                case Constants.ADDWORDS_VALUE:
                    ArrayList<String> wordsCustomList = new ArrayList<String>();
                    if (Constants.CUSTOM_FILE.exists()) {
                        RandomFileUtils.readWordsRandomFile(Constants.CUSTOM_FILE, wordsCustomList);
                    }
                    RandomFileUtils.addWordRandomFile(Constants.CUSTOM_FILE, wordsCustomList);
                    break;
                case Constants.INSTRUCTIONS_VALUE:
                    Display.writeln("\nEste ahorcado consiste en que el jugador tendrá que:\nAdivinar una palabra en un máximo de intentos según la dificultad.\nHay dos formas de jugar: con una lista dada por defecto o con una lista personalizada.\n");
                    Display.writeln("---------");
                    break;
            }
        } while (operationMenu != Constants.EXIT_VALUE);
    }

    /**
     * Obtiene la opción del menú.
     *
     * @return La opción elegida.
     */
    private static int getOperation() {
        int number = Constants.LOOP_VALUE;
        boolean rigthAnswer;
        //Display.writeln("---------");
        Display.writeln(Constants.NAME);
        Display.writeln(" " + Constants.PLAY_VALUE + ":" + Constants.PLAY_NAME);
        Display.writeln(" " + Constants.ADDWORDS_VALUE + ":" + Constants.ADDWORDS_NAME);
        Display.writeln(" " + Constants.INSTRUCTIONS_VALUE + ":" + Constants.INSTRUCTIONS_NAME);
        Display.writeln(" " + Constants.EXIT_VALUE + ":" + Constants.EXIT_NAME);
        Display.writeln("---------\n");
        do {
            try {
                do {
                    Display.write("--> Introduce tu opción: ");
                    number = new Scanner(System.in).nextInt();
                    if (number != Constants.PLAY_VALUE
                            && number != Constants.ADDWORDS_VALUE
                            && number != Constants.INSTRUCTIONS_VALUE
                            && number != Constants.EXIT_VALUE) {
                        Display.writeln("\nIntroduce un valor correcto\n");
                    }
                } while (number != Constants.PLAY_VALUE
                        && number != Constants.ADDWORDS_VALUE
                        && number != Constants.INSTRUCTIONS_VALUE
                        && number != Constants.EXIT_VALUE);
                rigthAnswer = true;
            } catch (final Exception e) {
                rigthAnswer = false;
                Display.writeln("\nIntroduce un valor correcto\n");
            }
        } while (!rigthAnswer);
        return number;
    }
}
