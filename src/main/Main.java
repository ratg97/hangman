package main;

import controller.GameController;
import utils.Constants;
import utils.RandomFileUtils;
import java.io.IOException;
import java.util.ArrayList;
import utils.TextFileUtils;

/**
 * Lanza el programa.
 *
 * @author Raúl Tejedor García
 * @version 1.0.0
 * @since 1.0.0
 */
public class Main {

    /**
     * Lanza el programa.
     *
     * @param args Los argumentos pasados por línea de comandos.
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        // Crea un fichero si el fichero no existe o si existe y está vacío.
        if (!Constants.DEFAULT_RANDOM_FILE.exists() || !(Constants.DEFAULT_RANDOM_FILE.length()>0) ) {
            ArrayList<String> wordsDefaultList = new ArrayList<String>();
            TextFileUtils.readTextFile(wordsDefaultList);
            RandomFileUtils.createFileFirstTime(Constants.DEFAULT_RANDOM_FILE, wordsDefaultList);
            //Borramos el ArrayList ya que no es necesario
            wordsDefaultList.clear();
        }
        GameController game = new GameController();
        game.showOptions();
    }
}
