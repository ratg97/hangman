package view;

import utils.Constants;

/**
 * Definición de los diferentes modos de mostrar mensajes.
 *
 * @author Raúl Tejedor García
 * @version 1.0.0
 * @since 1.0.0
 */
public class Display {

    /**
     * Constructor por defecto sin argumentos.
     */
    private Display() {
    }

    /**
     * Muestra el mensaje con salto de línea incluido.
     *
     * @param s La cadena a visualizar.
     */
    public static void writeln(final String s) {
        System.out.println(s);
    }

    /**
     * Muestra el mensaje sin salto de línea incluido.
     *
     * @param s La cadena a visualizar.
     */
    public static void write(final String s) {
        System.out.print(s);
    }

    /**
     * Muestra el mensaje con formato especial.
     *
     * @param direction La separación entre palabras.
     * @param distance La longitud que ocupa.
     * @param ln Salto de línea tras mensaje.
     * @param s La cadena a visualizar.
     */
    public static void writef(final boolean direction, final int distance, final boolean ln, final String s) {
        System.out.printf("%" + (direction ? '-' : "") + (distance < Constants.DEFAULT_SPACING || distance > Constants.SPACING_MAX ? Constants.DEFAULT_SPACING : distance) + "s" + (ln ? "%n" : "") + "", s);
    }
}
