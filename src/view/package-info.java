/**
 * Interfaz visual de interacción con el usuario.
 *
 * @author Raúl Tejedor García
 * @version 1.0.0
 * @since 1.0.0
 */
package view;