package model;

import view.Display;
import utils.Constants;
import utils.RandomFileUtils;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * Modela las características del 'ahorcado'
 *
 * @author Raúl Tejedor García
 * @version 1.0.0
 * @since 1.0.0
 */
public class Hangman {

    /**
     * Palabra del juego.
     */
    private String word;
    /**
     * Nivel del juego.
     */
    private int level;
    /**
     * Fichero a cargar.
     */
    private File file;

    /**
     * Constructor por defecto sin argumentos.
     */
    public Hangman() {
    }

    /**
     * Establece la palabra a resolver.
     *
     * @throws IOException
     */
    public void setWord() throws IOException {
        this.word = RandomFileUtils.readWordRandomFile(this.file);
    }

    /**
     * Establece el nivel del juego.
     */
    public void setLevel() {
        int number = Constants.LOOP_VALUE;
        boolean rightAnswer;
        Display.writeln("\n---------");
        Display.writeln("DIFICULTAD:");
        Display.writeln(" " + Constants.EASY_VALUE + ":" + Constants.EASY_NAME);
        Display.writeln(" " + Constants.MEDIUM_VALUE + ":" + Constants.MEDIUM_NAME);
        Display.writeln(" " + Constants.HARD_VALUE + ":" + Constants.HARD_NAME);
        Display.writeln("---------\n");
        do {
            try {
                do {
                    Display.write("--> Introduce el nivel: ");
                    number = new Scanner(System.in).nextInt();
                    if (number != Constants.EASY_VALUE
                            && number != Constants.MEDIUM_VALUE
                            && number != Constants.HARD_VALUE) {
                        Display.writeln("\nIntroduce un valor correcto\n");
                    }
                } while (number != Constants.EASY_VALUE
                        && number != Constants.MEDIUM_VALUE
                        && number != Constants.HARD_VALUE);
                rightAnswer = true;
            } catch (final Exception e) {
                rightAnswer = false;
                Display.writeln("\nIntroduce un valor correcto\n");
            }
        } while (!rightAnswer);
        this.level = number;
    }

    /**
     * Establece el fichero para cargarlo.
     */
    public void setFile() {
        int number = Constants.LOOP_VALUE;
        boolean rightAnswer;
        Display.writeln("\n---------");
        Display.writeln("ELIGE MODO:");
        Display.writeln(" " + Constants.DEFAULT_RANDOM_FILE_VALUE + ":" + Constants.DEFAULT_RANDOM_FILE_NAME);
        Display.writeln(" " + Constants.CUSTOM_FILE_VALUE + ":" + Constants.CUSTOM_FILE_NAME);
        Display.writeln("---------\n");
        do {
            try {
                do {
                    Display.write("--> Introduce el modo: ");
                    number = new Scanner(System.in).nextInt();
                    if (number != Constants.DEFAULT_RANDOM_FILE_VALUE
                            && number != Constants.CUSTOM_FILE_VALUE) {
                        Display.writeln("\nIntroduce un valor correcto\n");
                    }
                } while (number != Constants.DEFAULT_RANDOM_FILE_VALUE
                        && number != Constants.CUSTOM_FILE_VALUE);
                rightAnswer = true;
            } catch (final Exception e) {
                rightAnswer = false;
                Display.writeln("\nIntroduce un valor correcto\n");
            }
        } while (!rightAnswer);
        
        if (number == Constants.DEFAULT_RANDOM_FILE_VALUE) {
            this.file = Constants.DEFAULT_RANDOM_FILE;
        } else {
            this.file = Constants.CUSTOM_FILE;
        }
    }

    /**
     * Obtiene la palabra a resolver.
     *
     * @return Palabra a resolver.
     */
    public String getWord() {
        return word;
    }

    /**
     * Obtiene el nivel del juego.
     *
     * @return Nivel a jugar.
     */
    public int getLevel() {
        return level;
    }

    /**
     * Obtiene el fichero a cargar.
     *
     * @return Fichero a cargar.
     */
    public File getFile() {
        return file;
    }
}
