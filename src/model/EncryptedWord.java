package model;

import utils.Constants;
import java.util.ArrayList;

/**
 * Definición la palabra encriptada.
 *
 * @author Raúl Tejedor García
 * @version 1.0.0
 * @since 1.0.0
 */
public class EncryptedWord {

    /**
     * Palabra del juego encriptada.
     */
    private String word;

    /**
     * Constructor con argumentos.
     *
     * @param word La palabra a adivinar del ahorcado.
     */
    public EncryptedWord(String word) {
        this.word = word;
    }

    /**
     * Establece la palabara encriptada la primera vez durante la partida.
     *
     * @param letrasUsadas ArrayList con las letras usadas durante la partida.
     */
    public void setWordFirstTime(ArrayList<Character> letrasUsadas) {
        String encrypted = "";
        char characterHelp = this.word.charAt((int) (Math.random() * this.word.length()));
        int counterCharacter = countChars(characterHelp);
        if (!(this.word.length() == Constants.LENGTH_MIN_WORD)) {
            if (counterCharacter != this.word.length()) {
                letrasUsadas.add(characterHelp);
                for (int i = 0; i < this.word.length(); i++) {
                    if (this.word.charAt(i) == characterHelp) {
                        encrypted += this.word.charAt(i);
                    } else {
                        encrypted += Constants.ENCRYPTED_CHARACTER;
                    }
                }
            } else {
                for (int i = 0; i < this.word.length(); i++) {
                    encrypted += Constants.ENCRYPTED_CHARACTER;
                }
            }
        } else {
            for (int i = 0; i < this.word.length(); i++) {
                encrypted = "" + Constants.ENCRYPTED_CHARACTER;
            }
        }
        this.word = encrypted.toLowerCase();
    }

    /**
     * Establece la palabra a actualizar.
     *
     * @param word Palabra encriptada de la primera vez.
     */
    public void setWordUpdate(String word) {
        this.word = word;
    }

    /**
     * Obtenemos el estado actual de la palabra encriptada.
     *
     * @return La palabra encriptada.
     */
    public String getWord() {
        return word;
    }

    /**
     * Obtenemos el nº veces que una letra aparece en la palabra a adivinar.
     *
     * @param caracter Letra a buscar en la palabra.
     * @return Nº veces que aparece una letra en una palabra.
     */
    private int countChars(char character) {
        int pos, count = 0;
        pos = this.word.indexOf(character);
        while (pos != -1) {
            count++;
            pos = this.word.indexOf(character, pos + 1);
        }
        return count;
    }

    /**
     * Establece la palabra encriptada actualizándola o no.
     *
     * @param character Letra a buscar en la palabra del juego.
     * @param word Palabra a adivinar del juego.
     */
    public void setWordEcruptedUpdate(char character, String word) {
        String wordUpdate = "";
        for (int i = 0; i < this.word.length(); i++) {
            if (this.word.charAt(i) == word.charAt(i) || word.charAt(i) == character || (word.charAt(i) == 'á' && character == 'a') || (word.charAt(i) == 'é' && character == 'e') || (word.charAt(i) == 'í' && character == 'i') || (word.charAt(i) == 'ó' && character == 'o') || (word.charAt(i) == 'ú' && character == 'u') || (word.charAt(i) == 'ü' && character == 'u')) {
                wordUpdate += word.charAt(i);
            } else {
                wordUpdate += Constants.ENCRYPTED_CHARACTER;
            }
        }
        this.word = wordUpdate;
    }
}
