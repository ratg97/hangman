/**
 * Clases para las entidades del modelo del programa.
 *
 * @author Raúl Tejedor García
 * @version 1.0.0
 * @since 1.0.0
 */
package model;
