package model;

import view.Display;
import utils.Constants;

/**
 * Modela la representación gráfica del clásico juego 'ahorcado'.
 *
 * @author Raúl Tejedor García
 * @version 1.0.0
 * @since 1.0.0
 */
public class HangmanPicture {

    /**
     * Filas de la representación gráfica.
     */
    private static final int ROWS = 16;
    /**
     * Columnas de la representación gráfica.
     */
    private static final int COLUMNS = 16;
    /**
     * Dimensiones de la representación gráfica del verdugo.
     */
    private static final char[][] HANGMAN = new char[ROWS][COLUMNS];
    /**
     * Tamaño mínimo de la representación gráfica.
     */
    private static final int MIN_SIZE = 12;
    /**
     * Posición de las filas.
     */
    private static final int ROWS_POS = ROWS - 1;
    /**
     * Posición de las columnas.
     */
    private static final int COLUMNS_POS = COLUMNS - 1;
    /**
     * Carácter del palo vertical.
     */
    private static final char VERTICAL_STICK = '|';
    /**
     * Carácter del palo horizontal.
     */
    private static final char HORIZONTAL_STICK = '_';
    /**
     * Carácter del vacío.
     */
    private static final char VOID = ' ';
    /**
     * Carácter de la cabeza y el cuerpo.
     */
    private static final char HEAD_BODY = '|';
    /**
     * Carácter del pelo.
     */
    private static final char HAIR = (int) (Math.random() * 2) == 0 ? '/' : '-';
    /**
     * Carácter de los ojos vivo.
     */
    private static final char ALIVE_EYES = 'ʘ';
    /**
     * Carácter de la nariz.
     */
    private static final char NOSE = '.';
    /**
     * Carácter de los ojos muerto.
     */
    private static final char DEATH_EYES = 'X';
    /**
     * Carácter de la boca.
     */
    private static final char MOUTH = '_';
    /**
     * Carácter de la barbilla.
     */
    private static final char CHIN = '-';
    /**
     * Carácter del brazo izq.
     */
    private static final char LEFT_ARM = '/';
    /**
     * Carácter del brazo der.
     */
    private static final char RIGHT_ARM = '\\';
    /**
     * Carácter de la mano izq.
     */
    private static final char LEFT_HAND = '/';
    /**
     * Carácter de la mano der.
     */
    private static final char RIGHT_HAND = '\\';
    /**
     * Carácter de la pierna izq.
     */
    private static final char LEFT_LEG = '/';
    /**
     * Carácter de la pierna der.
     */
    private static final char RIGHT_LEG = '\\';
    /**
     * Carácter del pie izq.
     */
    private static final char LEFT_FOOT = '/';
    /**
     * Carácter del pie der.
     */
    private static final char RIGHT_FOOT = '\\';
    /**
     * Posición del palo vertical.
     */
    private static final int STICK_POS_VERTICAL = 0;
    /**
     * Posición del palo horizontal.
     */
    private static final int STICK_POS_HORIZONTAL = STICK_POS_VERTICAL;

    /**
     * Posición inicial horizontal del palo colgante.
     */
    private static final int STICK2_POS_HORIZONTAL_START = STICK_POS_VERTICAL + 1;
    /**
     * Posición final horizontal del palo colgante.
     */
    private static final int STICK2_POS_HORIZONTAL_FINISH = STICK2_POS_HORIZONTAL_START + 1;
    /**
     * Posición vertical del palo colgante.
     */
    private static final int STICK2_POS_VERTICAL_MARGIN = COLUMNS_POS - 3;
    /**
     * Posición inicial vertical del pelo.
     */
    private static final int HAIR_POS_VERTICAL_START = STICK2_POS_VERTICAL_MARGIN - 3;
    /**
     * Posición final vertical del pelo.
     */
    private static final int HAIR_POS_VERTICAL_FINISH = COLUMNS_POS;
    /**
     * Posición horizontal del pelo.
     */
    private static final int HAIR_POS_HORIZONTAL = STICK2_POS_HORIZONTAL_FINISH + 1;
    /**
     * Posición inicial horizontal de las orejas.
     */
    private static final int EAR_POS_HORIZONTAL_START = HAIR_POS_HORIZONTAL + 1;
    /**
     * Posición final horizontal de las orejas.
     */
    private static final int EAR_POS_HORIZONTAL_FINISH = EAR_POS_HORIZONTAL_START + 1;
    /**
     * Posición inicial vertical de las orejas.
     */
    private static final int EAR_POS_VERTICAL_START = HAIR_POS_VERTICAL_START + 1;
    /**
     * Posición final vertical de las orejas.
     */
    private static final int EAR_POS_VERTICAL_FINISH = HAIR_POS_VERTICAL_FINISH - 1;
    /**
     * Posición inicial vertical de la barbilla.
     */
    private static final int CHIN_POS_VERITCAL_START = HAIR_POS_VERTICAL_START;
    /**
     * Posición final vertical de la barbilla.
     */
    private static final int CHIN_POS_VERTICAL_FINISH = HAIR_POS_VERTICAL_FINISH;
    /**
     * Posición horizontal de la barbilla.
     */
    private static final int CHIN_POS_HORIZONTAL_FINISH = EAR_POS_HORIZONTAL_FINISH + 1;

    /**
     * Posición inicial horizontal de los ojos.
     */
    private static final int EYES_POS_HORIZONTAL_START = EAR_POS_HORIZONTAL_START;
    /**
     * Posición inicial vertical de los ojos.
     */
    private static final int EYES_POS_VERITCAL_START = EAR_POS_VERTICAL_START + 1;
    /**
     * Posición horizontal de la nariz.
     */
    private static final int NOSE_POS_HORIZONTAL = EYES_POS_HORIZONTAL_START;
    /**
     * Posición vertical de la nariz.
     */
    private static final int NOSE_POS_VERITCAL = EYES_POS_VERITCAL_START + 1;
    /**
     * Posición final vertical de los ojos.
     */
    private static final int EYES_POS_VERITCAL_FINISH = EAR_POS_VERTICAL_FINISH - 1;
    /**
     * Posición final horizontal de la boca.
     */
    private static final int MOUTH_POS_HORIZONTAL_FINISH = EYES_POS_HORIZONTAL_START + 1;
    /**
     * Posición vertical margen de la boca.
     */
    private static final int MOUTH_POS_VERITCAL_MARGIN = STICK2_POS_VERTICAL_MARGIN;
    /**
     * Posición vertical margen del cuerpo.
     */
    private static final int BODY_POS_VERTICAL_MARGIN = STICK2_POS_VERTICAL_MARGIN;
    /**
     * Posición inicial horizontal del cuerpo.
     */
    private static final int BODY_POS_HORIZONTAL_START = CHIN_POS_HORIZONTAL_FINISH + 1;
    /**
     * Posición final horizontal del cuerpo.
     */
    private static final int BODY_POS_HORIZONTAL_FINISH = MIN_SIZE > ROWS ? ROWS_POS : STICK2_POS_VERTICAL_MARGIN - 2;
    /**
     * Posición inicial vertical del brazo izq.
     */
    private static final int LEFT_ARM_POS_VERTICAL_START = STICK2_POS_VERTICAL_MARGIN - 1;
    /**
     * Posición final vertical del brazo izq.
     */
    private static final int LEFT_ARM_POS_VERTICAL_FINISH = LEFT_ARM_POS_VERTICAL_START - 1;
    /**
     * Posición inicial horizontal del brazo izq.
     */
    private static final int LEFT_ARM_POS_HORIZONTAL_START = BODY_POS_HORIZONTAL_START + 1;
    /**
     * Posición final horizontal del brazo izq.
     */
    private static final int LEFT_ARM_POS_HORIZONTAL_FINISH = LEFT_ARM_POS_HORIZONTAL_START + 1;
    /**
     * Posición final vertical de la mano izq.
     */
    private static final int LEFT_HAND_POS_VERTICAL_FINISH = LEFT_ARM_POS_VERTICAL_FINISH;
    /**
     * Posición inicial vertical del brazo der.
     */
    private static final int RIGHT_ARM_POS_VERTICAL_START = STICK2_POS_VERTICAL_MARGIN + 1;
    /**
     * Posición final vertical del brazo der.
     */
    private static final int RIGHT_ARM_POS_VERTICAL_FINISH = RIGHT_ARM_POS_VERTICAL_START + 1;
    /**
     * Posición inicial vertical del brazo der.
     */
    private static final int RIGHT_ARM_POS_HORIZONTAL_START = LEFT_ARM_POS_HORIZONTAL_START;
    /**
     * Posición final vertical del brazo der.
     */
    private static final int RIGHT_ARM_POS_HORIZONTAL_FINISH = LEFT_ARM_POS_HORIZONTAL_FINISH;
    /**
     * Posición final vertical de la mano der.
     */
    private static final int RIGHT_HAND_POS_VERTICAL_FINISH = RIGHT_ARM_POS_VERTICAL_FINISH;
    /**
     * Posición inicial vertical de la pierna izq.
     */
    private static final int LEFT_LEG_POS_VERTICAL_START = STICK2_POS_VERTICAL_MARGIN - 1;
    /**
     * Posición final vertical de la pierna izq.
     */
    private static final int LEFT_LEG_POS_VERTICAL_FINISH = LEFT_ARM_POS_VERTICAL_START - 1;
    /**
     * Posición inicial horizontal de la pierna izq.
     */
    private static final int LEFT_LEG_POS_HORIZONTAL_START = BODY_POS_HORIZONTAL_FINISH + 1;
    /**
     * Posición final horizontal de la pierna izq.
     */
    private static final int LEFT_LEG_POS_HORIZONTAL_FINISH = LEFT_LEG_POS_HORIZONTAL_START + 1;
    /**
     * Posición final vertical del pie izq.
     */
    private static final int LEFT_FOOT_POS_VERTICAL_FINISH = LEFT_LEG_POS_VERTICAL_FINISH;
    /**
     * Posición inicial vertical de la pierna der.
     */
    private static final int RIGHT_LEG_POS_VERTICAL_START = STICK2_POS_VERTICAL_MARGIN + 1;
    /**
     * Posición final vertical de la pierna der.
     */
    private static final int RIGHT_LEG_POS_VERTICAL_FINISH = RIGHT_ARM_POS_VERTICAL_START + 1;
    /**
     * Posición inicial horizontal de la pierna der.
     */
    private static final int RIGHT_LEG_POS_HORIZONTAL_START = LEFT_LEG_POS_HORIZONTAL_START;
    /**
     * Posición final horizontal de la pierna der.
     */
    private static final int RIGHT_LEG_POS_HORIZONTAL_FINISH = LEFT_LEG_POS_HORIZONTAL_FINISH;
    /**
     * Posición final vertical del pie der.
     */
    private static final int RIGHT_FOOT_POS_VERTICAL_FINISH = RIGHT_LEG_POS_VERTICAL_FINISH;

    /**
     * Constructor por defecto sin argumentos.
     */
    public HangmanPicture() {
    }

    /**
     * Actualiza el verdugo según los fallos del jugador y la dificultad del
     * juego.
     *
     * @param current_failures Fallos actuales del jugador.
     * @param level Nivel de la partida actual del jueog.
     */
    public void update(int current_failures, int level) {
        switch (current_failures) {
            case 0:
                updateStick();
                break;
            case 1:
                switch (level) {
                    case Constants.EASY_VALUE:
                        updateHead();
                        break;
                    case Constants.MEDIUM_VALUE:
                        updateHead();
                        updateBody();
                        break;
                    case Constants.HARD_VALUE:
                        updateHead();
                        updateBody();
                        updateArms();
                        break;
                }
                break;
            case 2:
                switch (level) {
                    case Constants.EASY_VALUE:
                        updateBody();
                        break;
                    case Constants.MEDIUM_VALUE:
                        updateArms();
                        break;
                    case Constants.HARD_VALUE:
                        updateLegs();
                        updateDeath();
                        break;
                }
                break;
            case 3:
                switch (level) {
                    case Constants.EASY_VALUE:
                        updateArms();
                        break;
                    case Constants.MEDIUM_VALUE:
                        updateLegs();
                        updateDeath();
                        break;
                }
                break;
            case 4:
                updateLegs();
                updateDeath();
                break;
        }
        show();
    }

    /**
     * Establece la representación gráfica con sólo los palos.
     */
    private static void updateStick() {
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                if (j == STICK_POS_VERTICAL && i != STICK_POS_HORIZONTAL) {
                    HANGMAN[i][j] = VERTICAL_STICK;
                } else {
                    if (i == STICK_POS_HORIZONTAL) {
                        HANGMAN[i][j] = HORIZONTAL_STICK;
                    } else {
                        if (j == STICK2_POS_VERTICAL_MARGIN && (i == STICK2_POS_HORIZONTAL_START || i == STICK2_POS_HORIZONTAL_FINISH)) {
                            HANGMAN[i][j] = VERTICAL_STICK;
                        } else {
                            HANGMAN[i][j] = VOID;
                        }
                    }
                }
            }
        }
    }

    /**
     * Establece la representación gráfica con la cabeza del verdugo.
     */
    private static void updateHead() {
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                if (j > HAIR_POS_VERTICAL_START && j < HAIR_POS_VERTICAL_FINISH && i == HAIR_POS_HORIZONTAL) {
                    HANGMAN[i][j] = HAIR;
                } else {
                    if (j == EAR_POS_VERTICAL_START && (i == EAR_POS_HORIZONTAL_START || i == EAR_POS_HORIZONTAL_FINISH)) {
                        HANGMAN[i][j] = HEAD_BODY;
                    } else {
                        if (j == EAR_POS_VERTICAL_FINISH && (i == EAR_POS_HORIZONTAL_START || i == EAR_POS_HORIZONTAL_FINISH)) {
                            HANGMAN[i][j] = HEAD_BODY;
                        } else {
                            if (j > CHIN_POS_VERITCAL_START && j < CHIN_POS_VERTICAL_FINISH && i == CHIN_POS_HORIZONTAL_FINISH) {
                                HANGMAN[i][j] = CHIN;
                            }
                        }
                    }
                }
            }
        }

        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                if (j == EYES_POS_VERITCAL_START && i == EYES_POS_HORIZONTAL_START) {
                    HANGMAN[i][j] = ALIVE_EYES;
                } else {
                    if (j == EYES_POS_VERITCAL_FINISH && i == EYES_POS_HORIZONTAL_START) {
                        HANGMAN[i][j] = ALIVE_EYES;
                    } else {
                        if (j == NOSE_POS_VERITCAL && i == NOSE_POS_HORIZONTAL) {
                            HANGMAN[i][j] = NOSE;
                        }
                    }

                }
            }
        }

        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                if (j == MOUTH_POS_VERITCAL_MARGIN && i == MOUTH_POS_HORIZONTAL_FINISH) {
                    HANGMAN[i][j] = MOUTH;
                }
            }
        }
    }

    /**
     * Establece la representación gráfica con el cuerpo del verdugo.
     */
    private static void updateBody() {
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                if (j == BODY_POS_VERTICAL_MARGIN && (i >= BODY_POS_HORIZONTAL_START && i <= BODY_POS_HORIZONTAL_FINISH)) {
                    HANGMAN[i][j] = HEAD_BODY;
                }
            }
        }
    }

    /**
     * Establece la representación gráfica con los brazos del verdugo.
     */
    private static void updateArms() {
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                if (j == RIGHT_ARM_POS_VERTICAL_START && i == RIGHT_ARM_POS_HORIZONTAL_START) {
                    HANGMAN[i][j] = RIGHT_ARM;
                } else {
                    if (j == RIGHT_HAND_POS_VERTICAL_FINISH && i == RIGHT_ARM_POS_HORIZONTAL_FINISH) {
                        HANGMAN[i][j] = RIGHT_HAND;
                    }
                }
            }
        }

        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                if (j == LEFT_ARM_POS_VERTICAL_START && i == LEFT_ARM_POS_HORIZONTAL_START) {
                    HANGMAN[i][j] = LEFT_ARM;
                } else {
                    if (j == LEFT_HAND_POS_VERTICAL_FINISH && i == LEFT_ARM_POS_HORIZONTAL_FINISH) {
                        HANGMAN[i][j] = LEFT_HAND;
                    }
                }
            }
        }
    }

    /**
     * Establece la representación gráfica con las piernas del verdugo.
     */
    private static void updateLegs() {
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                if (j == RIGHT_LEG_POS_VERTICAL_START && i == RIGHT_LEG_POS_HORIZONTAL_START) {
                    HANGMAN[i][j] = RIGHT_LEG;
                } else {
                    if (j == RIGHT_FOOT_POS_VERTICAL_FINISH && i == RIGHT_LEG_POS_HORIZONTAL_FINISH) {
                        HANGMAN[i][j] = RIGHT_FOOT;
                    }
                }
            }
        }

        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                if (j == LEFT_LEG_POS_VERTICAL_START && i == LEFT_LEG_POS_HORIZONTAL_START) {
                    HANGMAN[i][j] = LEFT_LEG;
                } else {
                    if (j == LEFT_FOOT_POS_VERTICAL_FINISH && i == LEFT_LEG_POS_HORIZONTAL_FINISH) {
                        HANGMAN[i][j] = LEFT_FOOT;
                    }
                }
            }
        }
    }

    /**
     * Establece la representación gráfica con la muerte del verdugo.
     */
    private static void updateDeath() {
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                if (j == EYES_POS_VERITCAL_START && i == EYES_POS_HORIZONTAL_START) {
                    HANGMAN[i][j] = DEATH_EYES;
                } else {
                    if (j == EYES_POS_VERITCAL_FINISH && i == EYES_POS_HORIZONTAL_START) {
                        HANGMAN[i][j] = DEATH_EYES;
                    }
                }
            }
        }

        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                if (j == MOUTH_POS_VERITCAL_MARGIN && i == MOUTH_POS_HORIZONTAL_FINISH) {
                    HANGMAN[i][j] = MOUTH;
                }
            }
        }
    }

    /**
     * Visualiza la representación gráfica.
     */
    private void show() {
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                Display.write(HANGMAN[i][j] + " ");
            }
            Display.writeln("");
        }
    }
}
