package controller;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import model.EncryptedWord;
import model.Hangman;
import model.HangmanPicture;
import utils.Constants;
import utils.GameUtils;
import utils.MenuUtils;
import view.Display;

/**
 * Ejecuta el algoritmo del 'ahorcado'.
 *
 * @author Raúl Tejedor García
 * @version 1.0.0
 * @since 1.0.0
 */
public class GameController {

    /**
     * Verdugo.
     */
    private Hangman game;
    /**
     * Estado de los ajustes.
     */
    private boolean isSettingsRight;

    /**
     * Constructor por defecto sin argumentos.
     */
    public GameController() {
        this.game = new Hangman();
    }

    /**
     * Establece los ajustes para la partida del ahorcado.
     *
     * @throws IOException
     */
    public void addSettigns() throws IOException {
        this.game.setFile();
        if (this.game.getFile().exists() && this.game.getFile().length()>0) {
            this.game.setLevel();
            this.game.setWord();
            this.isSettingsRight = true;
        } else {
            this.isSettingsRight = false;
            Display.writeln("--> Lo siento, no se puede jugar ya que no existe el fichero\n");
        }
    }

    /**
     * Obtenemos el estado de los ajustes de la partida del juego.
     *
     * @return true si todo está correcto, false si no.
     */
    public boolean isSettingsRight() {
        return isSettingsRight;
    }

    /**
     * Muestra las opciones del juego.
     *
     * @throws IOException
     */
    public void showOptions() throws IOException {
        MenuUtils.showOptions(this);
    }

    /**
     * Ejecuta el juego.
     *
     * @throws IOException
     */
    public void play() throws IOException {
        ArrayList<Character> charactersAvailable = new ArrayList<Character>();
        ArrayList<Character> charactersUsed = new ArrayList<Character>();
        GameUtils.addCharactersAvailable(charactersAvailable);

        EncryptedWord wordEncrypted = new EncryptedWord(this.game.getWord());
        wordEncrypted.setWordFirstTime(charactersUsed);
        EncryptedWord wordEncryptedUpdate = new EncryptedWord("");

        char characterPlayer;
        boolean continuePlaying = true;
        int currentFailures = 0;
        //Según la dificultad -> intentos
        int maxFailures = this.game.getLevel() == Constants.EASY_VALUE ? Constants.EASY_ATTEMPTS : this.game.getLevel() == Constants.MEDIUM_VALUE ? Constants.MEDIUM_ATTEMPTS : Constants.HARD_ATTEMPTS;
        boolean firstTime = true;

        HangmanPicture hangman = new HangmanPicture();
        hangman.update(currentFailures, this.game.getLevel());

        Display.writeln(wordEncrypted.getWord());
        while (continuePlaying && (currentFailures != maxFailures)) {
            ArrayList<Character> charactersIntroduced = new ArrayList<Character>();
            Display.writeln("Fallos permitidos: " + currentFailures + " de " + maxFailures);
            boolean estaLetra;
            do {
                GameUtils.showUsedCharacters(charactersUsed, charactersAvailable);
                characterPlayer = GameUtils.askCharacter();
                estaLetra = GameUtils.checkUsedCharacters(characterPlayer, charactersUsed);
                if (estaLetra) {
                    Display.writeln("\n--> Por favor introduce una letra no introducida\n");
                }
            } while (GameUtils.checkUsedCharacters(characterPlayer, charactersUsed));

            charactersUsed.add(characterPlayer);

            GameUtils.addIntroducedCharacters(characterPlayer, charactersIntroduced);

            //Si no existe la letra no actualizar...
            boolean encontrada = false;
            int i = 0;
            while (i < charactersIntroduced.size() && !encontrada) {
                if (this.game.getWord().indexOf((char) charactersIntroduced.get(i)) != -1) {
                    if (firstTime) {
                        wordEncrypted.setWordEcruptedUpdate(characterPlayer, this.game.getWord());
                        Display.writeln(wordEncrypted.getWord());
                    } else {
                        wordEncryptedUpdate.setWordEcruptedUpdate(characterPlayer, this.game.getWord());
                    }
                    encontrada = true;
                } else {
                    i++;
                }
            }
            if (!encontrada) {
                Display.writeln("\n--> La letra introducida no se encuentra en la palabra");
                currentFailures++;
            }

            if (firstTime) {
                wordEncryptedUpdate.setWordUpdate(wordEncrypted.getWord());
                firstTime = false;
            }

            if (wordEncryptedUpdate.getWord().equals(this.game.getWord()) || currentFailures == maxFailures) {
                continuePlaying = false;
                if (!(wordEncryptedUpdate.getWord().equals(this.game.getWord()))) {
                    hangman.update(currentFailures, this.game.getLevel());
                }
            } else {
                hangman.update(currentFailures, this.game.getLevel());
            }
            Display.writeln(wordEncryptedUpdate.getWord());
        }

        if (wordEncryptedUpdate.getWord()
                .equals(this.game.getWord())) {
            Display.writeln("\nVICTORIA!!!\n");
        } else {
            Display.writeln("\nDERROTA!!! ( ͡° ͜ʖ ͡°) \n");
            Display.writeln("La solución era: " + this.game.getWord() + "\n");
        }

        if (GameUtils.repeat()) {
            RandomAccessFile file = new RandomAccessFile(this.game.getFile(), "r");
            //Saber el nº palabras
            String oldWord = this.game.getWord();
            if ((int) (file.length() / Constants.SIZE_WORD) > 1) {
                do {
                    this.game.setWord();
                } while (this.game.getWord().equals(oldWord));
            } else {
                this.game.setWord();
            }
            play();
        } else {
            Display.writeln("---------\n");
        }
    }
}
