# **Proyecto NetBeans** #

El juego del **'Ahorcado'** para el idioma *Español*.

![Captura.PNG](https://bitbucket.org/repo/7EEKKnX/images/1078807545-Captura.PNG)

# **Metodología:** #

1. No Swing

1. MVC

1. Constantes

1. Expresiones regulares

1. Ficheros de texto

1. Ficheros de acceso aleatorio

1. Comentarios para Javadoc